// Copyright 2016 Zemtsov Artem

#include <gtest/gtest.h>
#include "../include/DatList.h"

// Testing type (int)
typedef struct ValueInt :TDatValue
{
    int Value;
    ValueInt() { Value = 0; }
    virtual TDatValue * GetCopy() { TDatValue *temp = new ValueInt; return temp; }
} *PValueInt;

// Initialization
class TestDatList : public ::testing::Test
{
protected:
    virtual void SetUp()
    {
        // Testing only elem
        Val = new ValueInt();
        Val->Value = 100;
        
        // Testing ten elems
        ArrVal = new PValueInt[N];
        for (int i = 0; i < N; i++)
        {
            ArrVal[i] = new ValueInt();
            ArrVal[i]->Value = i;
        }
    }
    virtual void TearDown()
    {
        delete ArrVal;
    }
    // Arrange
    const int N = 10;
    PValueInt Val;
    PValueInt *ArrVal;
    TDatList List;
};


TEST_F(TestDatList, correct_size_of_list)
{
    // Arg
    for (int i = 0; i < N; i++)
        List.InsLast(ArrVal[i]);
    
    // Assert
    EXPECT_EQ(List.GetListLength(), N);
}

TEST_F(TestDatList, can_delete_elem_from_beginnig)
{
    // Arg
    for (int i = 0; i < N; i++)
        List.InsLast(ArrVal[i]);
    // 0 1 2 3 4 5 6 7 8 9
    List.DelFirst();
    // 1 2 3 4 5 6 7 8 9
    
    // Assert
    EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[1]->Value);
    EXPECT_EQ(List.GetListLength(), N - 1);
}

TEST_F(TestDatList, can_add_an_item_to_the_end)
{
    // Arg
    List.InsLast(Val);
    
    // Assert
    PValueInt temp = (PValueInt)List.GetDatValue();
    EXPECT_EQ(Val->Value, temp->Value);
}


TEST_F(TestDatList, can_add_ten_elems_to_the_end)
{
    // Arg
    for (int i = 0; i < N; i++)
        List.InsLast(ArrVal[i]);
    
    // Assert
    for (int i = 0; i < N; i++, List.GoNext())
        EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[i]->Value);
}

TEST_F(TestDatList, can_add_ten_elems_to_the_beginnig)
{
    // Arg
    for (int i = 0; i < N; i++)
        List.InsFirst(ArrVal[i]);
    
    // Assert
    for (int i = N - 1; i > 0; i--, List.GoNext())
        EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[i]->Value);
}

TEST_F(TestDatList, can_add_ten_elems_to_the_current_pos)
{
    // Arg
    for (int i = 0; i < 5; i++)
        List.InsLast(ArrVal[i]);
    // 0 1 2 3 4
    // Current = 0
    List.GoNext();
    List.GoNext();
    // Current = 2
    List.InsCurrent(Val);
    // 0 1 100 2 3 4
    
    // Assert
    List.Reset();
    EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[0]->Value);
    List.GoNext();
    EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[1]->Value);
    List.GoNext();
    EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, Val->Value);
    List.GoNext();
    EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[2]->Value);
}

TEST_F(TestDatList, can_delete_elem_from_the_current_pos)
{
    // Arg
    for (int i = 0; i < N; i++)
        List.InsLast(ArrVal[i]);
    // 0 1 2 3 4 5 6 7 8 9
    List.GoNext();
    List.DelCurrent();
    // 0 2 3 4 5 6 7 8 9
    
    // Assert
    EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[2]->Value);
    EXPECT_EQ(List.GetListLength(), N - 1);
}
