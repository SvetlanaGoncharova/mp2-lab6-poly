//Copyright 2016 Zemtsov Artem

#include "../include/workWithUser.h"
#include <locale>

using namespace std;

int main() {
	setlocale(LC_ALL, "rus");
	Menu main_menu;
	main_menu.start();
	return 0;
}