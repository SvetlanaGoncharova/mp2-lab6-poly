#include "TDatList.h"

PTDatLink TDatList::getLink(PTDatValue pVal, PTDatLink pLink) {
	return new TDatLink(pVal, pLink);
}

void TDatList::delLink(PTDatLink pLink) {
	if (pLink != NULL)
	{
		if (pLink->pValue != NULL)
			delete pLink->pValue;
		delete pLink;
	}
}

TDatList:: TDatList():ListLen (0) {
	pFirst = pLast = pStop = NULL;
	reset();
}


PTDatValue TDatList::getDatValue(TLinkPos mode) const {
	PTDatLink temp=NULL;
	switch (mode) {
	case FIRST: temp = pFirst; break;
	case CURRENT: temp = pCurrLink; break;
	case LAST: temp = pLast; break;
	}
	return (temp == NULL) ? NULL : temp->getDatValue();
}

int TDatList::getCurrentPos(void) const {
	return CurrPos;
}

int TDatList::reset(void) {
	pPrevLink = pStop;
	isEmpty() ? (CurrPos = -1, pCurrLink = pStop) : (CurrPos = 0, pCurrLink = pFirst);
	return 0;
}

int TDatList::goNext(void) {
	return isListEnded() ? 0 : pPrevLink = pCurrLink, pCurrLink = pCurrLink->getNextDatLink(), CurrPos++, 1;
}

int TDatList::setCurrentPos(int pos) {
	reset();
	for (int i = 0; i < pos; i++, goNext())
		;
	return 0;
}

int TDatList::isListEnded(void) const {
	return pCurrLink == pStop;
}

void TDatList:: insFirst(PTDatValue pVal) {
	PTDatLink temp = getLink(pVal, pFirst);
	if (temp) {
		temp->setNextLink(pFirst);
		pFirst= temp;
		ListLen++;
		
		if (ListLen == 1) {
			pLast = temp;
			reset();
		} else if (CurrPos == 0)
			pCurrLink = temp;
		else
			CurrPos++;
	}
}

void TDatList:: insLast(PTDatValue pVal) {
	PTDatLink temp = getLink(pVal, pStop);
	if (temp) {
		if (pLast)
			pLast->setNextLink(temp);
			
		pLast = temp;
		ListLen++;
		
		if (ListLen == 1) {
			pFirst = temp;
			reset();
		}
		
		if (isListEnded())
			pCurrLink = temp;
	}
}

void TDatList:: insCurrent(PTDatValue pVal) {
	if ((pCurrLink == pFirst) || isEmpty())
		insFirst(pVal);
	else if (isListEnded())
			insLast(pVal);
	else {
		PTDatLink temp = getLink(pVal, pCurrLink);
		if (temp) {
			pPrevLink->setNextLink(temp);
			temp->setNextLink(pCurrLink);
			pCurrLink = temp;
			ListLen++;
		}
	}
}

void TDatList:: delFirst(void) {
	if (!isEmpty()) {
		PTDatLink temp = pFirst;
		pFirst = pFirst->getNextDatLink();
		delLink(temp);
		ListLen--;
		if (isEmpty()) {
			pLast = pStop;
			reset();
		}
		else if (CurrPos == 0) pCurrLink = pFirst;
		else if (CurrPos == 1) pPrevLink = pStop;
		if (CurrPos) CurrPos--;
	}
}

void TDatList::delCurrent(void) {
	if (pCurrLink) {
		if ((pCurrLink == pFirst) || isEmpty())
			delFirst();
		else {
			PTDatLink temp = pCurrLink;
			pCurrLink = pCurrLink->getNextDatLink();
			pPrevLink->setNextLink(pCurrLink);
			delLink(temp);
			ListLen--;

			if (pCurrLink == pLast) {
				pLast = pPrevLink;
				pCurrLink = pStop;
			}
		}
	}
}

void TDatList::delList(void) {
	while (!isEmpty()) delFirst();
	pFirst = pLast = pPrevLink = pCurrLink = pCurrLink = pStop;
	CurrPos = -1;
}