#pragma once
#include "TRootLink.h"

class TDatLink;
typedef TDatLink *PTDatLink;

class TDatLink : public TRootLink {
protected:
	PTDatValue pValue;  // a pointer to the object values
public:
	TDatLink(PTDatValue pVal = NULL, PTRootLink pN = NULL) :TRootLink(pN) {
		pValue = pVal;
	}
	void setDatValue(PTDatValue pVal) { pValue = pVal; }
	PTDatValue getDatValue() { return  pValue; }
	PTDatLink  getNextDatLink() { return  (PTDatLink)pNext; }
	friend class TDatList;
};
