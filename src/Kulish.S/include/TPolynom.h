#pragma once
#include "THeadRing.h"
#include "TMonom.h"
#include <iostream>
#include <string>
#include <boost/lexical_cast.hpp>
#include <regex>
#include <exception>

using namespace std;
using boost::lexical_cast;

enum Variables {z = 1, y  = 10, x = 100};

class TPolynom : public THeadRing {
public:
	TPolynom(int monoms[][2] = NULL, int km = 0); // constructor
	
	// methods
	TPolynom(TPolynom &q);      // copy constructor
	PTMonom  getMonom() { return (PTMonom)getDatValue(); }
	TPolynom operator+(TPolynom &q);   // polynom addition
	TPolynom & operator=(TPolynom &q); // polynom assignment

	// own methods
	TPolynom derivative(Variables v = z);					// finding the derivative
	bool operator==(TPolynom &q);							// polynom comparison
	friend ostream & operator<<(ostream &os, TPolynom &q);	// input
	friend istream & operator>>(istream &os, TPolynom &q);	// output
};