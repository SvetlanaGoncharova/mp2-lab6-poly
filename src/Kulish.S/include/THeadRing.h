#pragma once
#include "TDatList.h"
class THeadRing : public TDatList {
protected:
	PTDatLink pHead;
public:
	THeadRing();
	~THeadRing();

	virtual void insFirst(PTDatValue pVal = NULL);

	virtual void delFirst(void);
};
