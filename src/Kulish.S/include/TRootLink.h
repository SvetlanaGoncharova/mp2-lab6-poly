#pragma once
#include <iostream>
#include "TDatValue.h"

class TRootLink;
typedef TRootLink *PTRootLink;

class TRootLink {
protected:
	PTRootLink  pNext;  // pointer to the next link
public:
	TRootLink(PTRootLink pN = NULL) { pNext = pN; }
	PTRootLink  getNextLink() { return  pNext; }
	void setNextLink(PTRootLink  pLink) { pNext = pLink; }
	void insNextLink(PTRootLink  pLink) {
		PTRootLink p = pNext;  pNext = pLink;
		if (pLink != NULL) pLink->pNext = p;
	}
	virtual void setDatValue(PTDatValue pVal) = 0;
	virtual PTDatValue getDatValue() = 0;

	friend class TDatList;
};
