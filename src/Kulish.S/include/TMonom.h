#pragma once
#include "TDatValue.h"

class TMonom : public TDatValue {
protected:
	int Coeff; // monom coeficient
	int Index; // index 
public:
	TMonom(int cval = 1, int ival = 0) {
		Coeff = cval; Index = ival;
	};
	virtual TDatValue * getCopy() { TDatValue * temp = new TMonom(Coeff, Index); return temp; } // make copy
	void setCoeff(int cval) { Coeff = cval; }
	int  getCoeff(void) { return Coeff; }
	void setIndex(int ival) { Index = ival; }
	int  getIndex(void) { return Index; }
	TMonom& operator=(const TMonom &tm) {
		Coeff = tm.Coeff; Index = tm.Index;
		return *this;
	}
	int operator==(const TMonom &tm) const {
		return (Coeff == tm.Coeff) && (Index == tm.Index);
	}
	int operator!=(const TMonom &tm) const {
		return (Coeff != tm.Coeff) || (Index != tm.Index);
	}
	int operator<(const TMonom &tm) const {
		return Index<tm.Index;
	}
	friend class TPolynom;
};
typedef TMonom *PTMonom;
