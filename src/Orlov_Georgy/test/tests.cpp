﻿#include "gtest.h"
#include "Polinom.h"
#include "DatList.h"

// Tpolinom tests
TEST(TPolinom, Can_Create_Polynom_From_One_Monom)
{
    int mon[][2] = { { 3, 6 } };

    TPolinom Pol(mon, 1);

    TMonom expected_monom(3, 6);
    EXPECT_EQ(expected_monom, *Pol.GetMonom());
}

TEST(TPolinom, Can_Create_Polynom_From_Five_Monoms)
{
    const int size = 5;
    int mon[][2] = { { -2, 92 },{ 1, 53 },{ 1, 6 },{ 72, 808 },{ 7, 14 } };

    TPolinom Pol(mon, size);

    TMonom monoms[size];
    for (int i = 0; i < size; i++)
        monoms[i] = TMonom(mon[i][0], mon[i][1]);
    for (int i = 0; i < size; i++, Pol.GoNext())
        EXPECT_EQ(monoms[i], *Pol.GetMonom());
}

TEST(TPolinom, Can_Right_Compare_the_Polynoms)
{
    const int size = 2;
    int mon[][2] = { { -2, 52 },{ 2, 454 }};
    TPolinom Pol1(mon, size);
    TPolinom Pol2(mon, size);

    EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, Can_Right_Assign_Polynoms)
{
    const int size = 2;
    int mon[][2] = { { 51, 822 },{ -27, 44 } };
    TPolinom Pol1(mon, size);
    TPolinom Pol2;

    Pol2 = Pol1;

    EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, Can_Right_Copy_the_Polynoms)
{
    const int size = 2;
    int mon[][2] = { { 51, 822 },{ -27, 44 } };
    TPolinom Pol1(mon, size);

    TPolinom Pol2 = Pol1;

    EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, Can_Add_Linear_Polynoms)
{
    const int size = 1;
    int mon1[][2] = { { 7, 1 } };
    int mon2[][2] = { { 2, 1 } };
    TPolinom Pol1(mon1, size); // 7z
    TPolinom Pol2(mon2, size); // 2z

    TPolinom Pol = Pol1 + Pol2;

    const int expected_size = 1;
    int expected_mon[][2] = { { 9, 1 } };
    TPolinom expected_Pol(expected_mon, expected_size);
    EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, Can_Add_Polynoms)
{
    const int size1 = 3;
    const int size2 = 2;
    int mon1[][2] = { { 10, 234 },{ -15, 500 },{ 30, 999 } };
    int mon2[][2] = { { 5, 500 },{ 3, 712 } };
    TPolinom Pol1(mon1, size1); // 10x^2y^3z^4-15x^5+30x^9y^9z^9
    TPolinom Pol2(mon2, size2); // 5x^5+3x^7y^1z^2

    TPolinom Pol = Pol1 + Pol2;

    const int expected_size = 4;
    int expected_mon[][2] = { { 10, 234 }, { -10, 500 }, { 3, 712 }, { 30, 999 } };
    // 10x^2y^3z^4-10x^5+3x^7y^1z^2+30x^9y^9z^9
    TPolinom expected_Pol(expected_mon, expected_size);
    EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, Can_Add_Four_Polynoms)
{
    // Arrange
    const int size1 = 2;
    const int size2 = 3;
    const int size3 = 4;
    const int size4 = 5;
    int mon1[][2] = { { -3, 1 }, { 8, 2 } };
    int mon2[][2] = { { 4, 1 }, { 1, 2 }, { 6, 3 } };
    int mon3[][2] = { { 4, 1 }, { -8, 2 }, { -1, 3 }, { 1, 4 } };
    int mon4[][2] = { { 0, 1 }, { 2, 2 }, { -1, 3 }, { 2, 4 }, { 1, 5 } };
    TPolinom Pol1(mon1, size1); // -3z+8z^2
    TPolinom Pol2(mon2, size2); // 4z+z^2+6z^3
    TPolinom Pol3(mon3, size3); // 4z-8z^2-z^3+z^4
    TPolinom Pol4(mon4, size4); //    2z^2-z^3+2z^4+z^5

    TPolinom Pol = Pol1 + Pol2 + Pol3 + Pol4;

    const int expected_size = 5;
    int expected_mon[][2] = { { 5, 1 }, { 3, 2 }, { 4, 3 }, { 3, 4 }, { 1, 5 } };
    // 5z+3z^2+4z^3+3z^4+z^5
    TPolinom expected_Pol(expected_mon, expected_size);
    EXPECT_TRUE(Pol == expected_Pol);
}

// Testing type (int)
typedef struct ValueInt :TDatValue
{
    int Value;
    ValueInt() { Value = 0; }
    virtual TDatValue * GetCopy() { TDatValue *temp = new ValueInt; return temp; }
} *PValueInt;

class TestList : public ::testing::Test
{
protected:
    virtual void SetUp()
    {
        // Testing only elem
        Val = new ValueInt();
        Val->Value = 100;

        // Testing ten elems 
        ArrVal = new PValueInt[N];
        for (int i = 0; i < N; i++)
        {
            ArrVal[i] = new ValueInt();
            ArrVal[i]->Value = i;
        }
    }
    virtual void TearDown()
    {
        delete ArrVal;
    }
    // Arrange
    const int N = 5;
    PValueInt Val;
    PValueInt *ArrVal;
    TDatList List;
};

TEST_F(TestList, Can_Add_An_Item_To_the_End)
{
    List.InsLast(Val);
    PValueInt temp = (PValueInt)List.GetDatValue();
    EXPECT_EQ(Val->Value, temp->Value);
}

TEST_F(TestList, Can_Right_Get_Size_Of_List)
{
    for (int i = 0; i < N; i++)
        List.InsLast(ArrVal[i]);
    EXPECT_EQ(List.GetListLength(), N);
}

TEST_F(TestList, Can_Add_Five_Elements_To_End_On_List)
{
    // Arg
    for (int i = 0; i < N; i++)
        List.InsLast(ArrVal[i]);

    // Assert
    for (int i = 0; i < N; i++, List.GoNext())
        EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[i]->Value);
}
