#include "Polinom.h"

TPolinom::TPolinom(int monoms[][2], int km)
{
    pHead->SetDatValue(new TMonom(0, -1));
    for (int i = 0; i < km; i++)
        InsLast(new TMonom(monoms[i][0], monoms[i][1]));
}

TPolinom::TPolinom(TPolinom &q)
{
    *this = q;
}

TPolinom TPolinom::operator+(TPolinom & q)
{
    TPolinom result;
    PTMonom res_monom = nullptr;
    TMonom mon, mon_q;
    const TMonom null_mon(0, -1);

    Reset();
    q.Reset();

    while (!IsListEnded() || !q.IsListEnded()) {
        mon = IsListEnded() ? null_mon : *GetMonom();
        mon_q = q.IsListEnded() ? null_mon : *q.GetMonom();

        if (mon == null_mon) {
            res_monom = new TMonom(mon_q.GetCoeff(), mon_q.GetIndex());
            q.GoNext();
        }
        else if (mon_q == null_mon) {
            res_monom = new TMonom(mon.GetCoeff(), mon.GetIndex());
            GoNext();
        }

        else if (mon < mon_q) {
            res_monom = new TMonom(mon.GetCoeff(), mon.GetIndex());
            GoNext();
        }
        else if (mon_q < mon) {
            res_monom = new TMonom(mon_q.GetCoeff(), mon_q.GetIndex());
            q.GoNext();
        }

        else if (mon.GetIndex() == mon_q.GetIndex()) {
            res_monom = new TMonom(mon.GetCoeff() + mon_q.GetCoeff(), mon.GetIndex());
            GoNext();
            q.GoNext();
        }

        else
            break;

        if (res_monom->GetCoeff())
            result.InsLast(res_monom);
    }

    return result;
}

TPolinom & TPolinom::operator=(TPolinom & q)
{
    DelList();

    if ((&q != nullptr) && (&q != this)) {
        PTMonom Mon = new TMonom(0, -1);
        pHead->SetDatValue(Mon);

        for (q.Reset(); !q.IsListEnded(); q.GoNext()) {
            Mon = q.GetMonom();
            InsLast(Mon->GetCopy());
        }
    }

    return *this;
}

bool TPolinom::operator==(TPolinom & q)
{
    bool result = false;

    if (q.GetListLength() == GetListLength())
    {
        result = true;
        for (q.Reset(), Reset(); !q.IsListEnded(); q.GoNext(), GoNext())
            if (*(PTMonom)q.GetDatValue() != *(PTMonom)GetDatValue())
            {
                result = false;
                break;
            }
    }

    return result;
}

double TPolinom::Calc(double x, double y, double z)
{
    double result = 0.0;
    int coeff, index;
    int x_index, y_index, z_index;

    for (Reset(); !IsListEnded(); GoNext())
    {
        coeff = GetMonom()->GetCoeff();
        index = GetMonom()->GetIndex();

        x_index = index / 100;
        y_index = (index % 100) / 10;
        z_index = index % 10;

        result += coeff * pow(x, x_index) * pow(y, y_index) * pow(z, z_index);
    }

    return result;
}

ostream & operator<<(ostream & os, TPolinom & q) {

    string p_out = "";
    string buf = "";
    string sign;

    int coef, ind, ind_x, ind_y, ind_z = 0;

    for (q.Reset(); !q.IsListEnded(); q.GoNext())
    {
        coef = q.GetMonom()->GetCoeff();
        ind = q.GetMonom()->GetIndex();
        ind_x = ind / 100;
        ind_y = (ind % 100) / 10;
        ind_z = ind % 10;

        sign = "+";
        if (coef < 0)
            sign = "-";
        buf = to_string(abs(coef));

        if (ind_x)
            if (ind_x != 1)
                buf = buf + "x^" + to_string(ind_x);
            else
                buf = buf + "x";
        if (ind_y)
            if (ind_y != 1)
                buf = buf + "y^" + to_string(ind_y);
            else
                buf = buf + "y";
        if (ind_z)
            if (ind_z != 1)
                buf = buf + "z^" + to_string(ind_z);
            else
                buf = buf + "z";

        if (p_out != "")
            p_out = p_out + " " + sign + " " + buf;
        else
        {
            if (sign == "-")
                p_out += sign;
            p_out += buf;
        }
    }
    os << p_out << endl;;
    return os;
}

typedef regex_iterator<string::iterator> inter;

istream & operator>>(istream & os, TPolinom & q) // Copyright 2016 Petrov Kirill
{
    // ��������������� ����������
    int i = 0, coeff, index;
    string buf;

    // ���� "��������"
    os >> buf;

    PTMonom mon;
    // ��������� ��������� �� ���������� ������� ������
    regex re_input("(([\\+|-]|^)[1-9](\\d+)?x\\^\\dy\\^\\dz\\^\\d)+");
    // �������� ��� ����������� � ������
    regex re_coeff("^\\d+|(\\+|-)((\\d+))");
    // �������� ��� ������� �� ^
    regex re_index("(\\^\\d)");

    // ��� ���������, �� ������ ��������� ���������� ���������
    inter it_index(buf.begin(), buf.end(), re_index);
    inter it_coeff(buf.begin(), buf.end(), re_coeff);

    // �������� ��� ���������� ��������� � ������ �� ���� ������ �����
    for (; it_coeff != inter(); ++it_coeff, ++i)
    {
        // �������� ������������
        coeff = lexical_cast <int>(it_coeff->str());

        // ������� ����������� ������ ������ x*100+y*10+z*1
        index = lexical_cast <int>(it_index++->str()[1]) * 100 +  //x
            lexical_cast <int>(it_index++->str()[1]) * 10 +       //y
            lexical_cast <int>(it_index++->str()[1]);             //z

                                                                    // ������ ����� � ������� � ����� ������
        mon = new TMonom(coeff, index);
        q.InsLast(mon);
    }
    return os;

}
