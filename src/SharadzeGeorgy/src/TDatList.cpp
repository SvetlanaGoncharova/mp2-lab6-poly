#include "../include/TDatList.h"

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink) {
	return new TDatLink(pVal, pLink);
}

void TDatList::DelLink(PTDatLink pLink) {
	if (pLink->pValue != NULL)
		delete pLink->pValue;
	delete pLink;
}

TDatList::TDatList() {
	ListLen=0;
	pFirst = NULL;
	pCurrLink = NULL;
	pLast = NULL;
	Reset();
}

int TDatList::Reset() {
	pPrevLink = pStop;
	if (!IsEmpty()) {
		pCurrLink = pFirst;
		CurrPos = 0;
	} else {
		pCurrLink = pStop;
		CurrPos = -1;
	}
	return 0;
}

PTDatValue TDatList::GetDatValue(TLinkPos mode) const {
	PTDatLink temp = NULL;
	switch (mode) {
	case FIRST: 
		temp = pFirst; 
		break;
	case CURRENT: 
		temp = pCurrLink; 
		break;
	case LAST: 
		temp = pLast; 
		break;
	}
	if (temp != NULL)
		return temp->GetDatValue();
	else
		return NULL;
}

int TDatList::GoNext() {
	if (!IsListEnded()) {
		pPrevLink = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		CurrPos++;
		return 1;
	}
	else
		return 0;
}


int TDatList::SetCurrentPos(int pos) {
	Reset();
	for (int i = 0; i < pos; i++)
		GoNext();
	return 0;
}

int TDatList::GetCurrentPos() const {
	return CurrPos;
}

int TDatList::IsListEnded() const {
	return pCurrLink == pStop;
}

void TDatList::InsFirst(PTDatValue pVal) {
	PTDatLink temp = GetLink(pVal, pFirst);
	if (temp) {
		temp->SetNextLink(pFirst);
		pFirst = temp;
		ListLen++;
		if (ListLen == 1) {
			pLast = temp;
			Reset();
		}
		else if (CurrPos == 0)
			pCurrLink = temp;
		else
			CurrPos++;
	}	
}

void TDatList::InsLast(PTDatValue pVal) {
	PTDatLink tmp = GetLink(pVal, pStop);
	if (tmp) {
		if (pLast)
			pLast->SetNextLink(tmp);
		pLast = tmp;
		ListLen++;
		if (ListLen == 1) {
			pFirst = tmp;
			Reset();
		}
		if (IsListEnded())
			pCurrLink = tmp;
	}
}

void TDatList::InsCurrent(PTDatValue pVal) {
	if ((pCurrLink == pFirst) || IsEmpty())
		InsFirst(pVal);
	else if (IsListEnded())
		InsLast(pVal);
	else {
		PTDatLink tmp = GetLink(pVal, pCurrLink);
		if (tmp) {
			pPrevLink->SetNextLink(tmp);
			tmp->SetNextLink(pCurrLink);
			ListLen++;
			pCurrLink = tmp;
		}
	}
}

void TDatList::DelFirst() {
	if (!IsEmpty())	{
		PTDatLink tmp = pFirst;
		pFirst = pFirst->GetNextDatLink();
		DelLink(tmp);
		ListLen--;
		if (IsEmpty()) {
			pLast = pStop;
			Reset();
		}
		else if (CurrPos == 1)
			pCurrLink = pStop;
		else if (CurrPos == 0)
			pCurrLink = pFirst;
		if (CurrPos) 
			CurrPos--;
	}
}

void TDatList::DelCurrent() {
	if (pCurrLink) {
		if ((pCurrLink == pFirst) || IsEmpty())
			DelFirst();
		else {
			PTDatLink tmp = pCurrLink;
			pCurrLink = pCurrLink->GetNextDatLink();
			pPrevLink->SetNextLink(pCurrLink);
			DelLink(tmp);
			ListLen--;
			if (pCurrLink == pLast) {
				pLast = pPrevLink;
				pCurrLink = pStop;
			}
		}
	}
}

void TDatList::DelList() {
	while (!IsEmpty())
		DelFirst();
	CurrPos = -1;
	pFirst = pLast = pPrevLink = pCurrLink = pCurrLink = pStop;
}
