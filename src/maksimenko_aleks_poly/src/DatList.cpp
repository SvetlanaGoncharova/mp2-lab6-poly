//Copyright 2016 Maksmienko Aleksey

#include "DatList.h"

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
{
    return new TDatLink(pVal, pLink);
}

void TDatList::DelLink(PTDatLink pLink)   //                      �������� �����.
{
    delete pLink;
}

TDatList::TDatList() //                                           �����������.
{
    ListLen = NULL;
    pFirst = pCurrLink = pLast = NULL;
    Reset();
}

PTDatValue TDatList::GetDatValue(TLinkPos mode) const //          ��������.
{
    PTDatLink tmp = NULL;
    switch (mode)
    {
    case FIRST:
        tmp = pFirst;
        break;
    case CURRENT:
        tmp = pCurrLink;
        break;
    case LAST:
        tmp = pLast;
        break;
    }
    if (tmp != NULL)
        return tmp->GetDatValue();
    else
        return NULL;
}

int TDatList::SetCurrentPos(int pos) //                           ���������� ������� �����.
{
    Reset();
    for (int i = 0; i < pos; i++)
        GoNext();
    return 0;
}

int TDatList::GetCurrentPos(void) const //                        �������� ����� �������� �����.
{
    return CurrPos;
}

int TDatList::Reset(void) //                                      ���������� �� ������ ������.
{
    pPrevLink = pStop;
    if (!IsEmpty())
    {
        pCurrLink = pFirst;
        CurrPos = 0;
    }
    else
    {
        pCurrLink = pStop;
        CurrPos = -1;
    }
    return 0;
}

int TDatList::IsListEnded(void) const //                          ������ �������� ?
{
    return pCurrLink == pStop;
}

int TDatList::GoNext(void) //                                     ����� ������ �������� �����.
{
    if (!IsListEnded())
    {
        pPrevLink = pCurrLink;
        pCurrLink = pCurrLink->GetNextDatLink();
        CurrPos++;
        return 1;
    }
    else
        return 0;
}

void TDatList::InsFirst(PTDatValue pVal) //                       ����� ������.
{
    PTDatLink tmp = GetLink(pVal, pFirst);
    if (tmp)
    {
        tmp->SetNextLink(pFirst);
        pFirst = tmp;
        ListLen++;
        if (ListLen == 1)
        {
            pLast = tmp;
            Reset();
        }
        else if (CurrPos == 0)
            pCurrLink = tmp;
        else
            CurrPos++;
    }

}

void TDatList::InsLast(PTDatValue pVal) //                        �������� ���������.
{
    PTDatLink tmp = GetLink(pVal, pStop);
    if (tmp)
    {
        if (pLast)
            pLast->SetNextLink(tmp);
        pLast = tmp;
        ListLen++;
        if (ListLen == 1)
        {
            pFirst = tmp;
            Reset();
        }
        if (IsListEnded())
            pCurrLink = tmp;
    }

}

void TDatList::InsCurrent(PTDatValue pVal) //                     ����� �������.
{
    if ((pCurrLink == pFirst) || IsEmpty())
        InsFirst(pVal);
    else if (IsListEnded())
        InsLast(pVal);
    else
    {
        PTDatLink tmp = GetLink(pVal, pCurrLink);
        if (tmp)
        {
            pPrevLink->SetNextLink(tmp);
            tmp->SetNextLink(pCurrLink);
            ListLen++;
            pCurrLink = tmp;
        }
    }
}

void TDatList::DelFirst(void) //                                  ������� ������ �����.
{
    if (!IsEmpty())
    {
        PTDatLink tmp = pFirst;
        pFirst = pFirst->GetNextDatLink();
        DelLink(tmp);
        ListLen--;
        if (IsEmpty())
        {
            pLast = pStop;
            Reset();
        }
        else if (CurrPos == 1)
            pCurrLink = pStop;
        else if (CurrPos == 0)
            pCurrLink = pFirst;
        if (CurrPos)
            CurrPos--;
    }
}

void TDatList::DelCurrent(void) //                                ������� ������� �����.
{
    if (pCurrLink)
    {
        if ((pCurrLink == pFirst) || IsEmpty())
            DelFirst();
        else
        {
            PTDatLink tmp = pCurrLink;
            pCurrLink = pCurrLink->GetNextDatLink();
            pPrevLink->SetNextLink(pCurrLink);
            DelLink(tmp);
            ListLen--;
            if (pCurrLink == pLast)
            {
                pLast = pPrevLink;
                pCurrLink = pStop;
            }
        }
    }
}

void TDatList::DelList(void) //                                    ������� ���� ������.
{
    while (!IsEmpty())
        DelFirst();
    CurrPos = -1;
    pFirst = pLast = pPrevLink = pCurrLink = pCurrLink = pStop;
}
