#include <iostream>
#include "Polinom.h"
#include <string>
#include <locale>

//#define TEST

#ifdef TEST

#include "gtest/gtest.h"

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
#else
int main() {
    setlocale(LC_ALL, "rus");
    TPolinom Poly, expected_Poly, result_Poly;
    char YN;
    double _x = 0, _y = 0, _z = 0, result = 0;

    cout << "���� �������� ���������� � ���������������� ������ �����," << endl;
    cout << "������� ������ ������������� ��������� �����������:" << endl;
    cout << "    1) ���� ����� ������ ���� ��� ��������;" << endl;
    cout << "    2) ��� ����� ��������, �������� ������ ������� � ���������� [0,9] " << endl;
    cout << "���� �� �� ��������� ���� ��������(�����������, ������� x, ������� y, ������� z) " << endl;
    cout << " - ���������� ����� ������ ������� ��������." << endl;

    do
    {
        Poly.DelList();
        cout << "������� ������ ������� " << endl;
        cin >> Poly;
        cout << "���������� �� ����� ������� ��������� � ����� ��������? Y/N" << endl;
        cout << Poly << endl;
        cin >> YN;
    } while (YN == 'N');

    do
    {
        expected_Poly.DelList();
        cout << "������� ������ ������� " << endl;
        cin >> expected_Poly;
        cout << "���������� �� ����� ������� ��������� � ����� ��������? (Y/N): " << endl;
        cout << expected_Poly << endl;
        cin >> YN;
    } while (YN == 'N');

    cout << "����� ���� ��������� �����: " << endl;
    result_Poly = Poly + expected_Poly;
    cout << (result_Poly) << endl;
    cout << endl;

    cout << " �������� ���������� x, y, z ��������? (Y/N):" << endl;
    cin >> YN;
    switch (YN) {
    case 'Y':
        cout << "������� �������� ���������� x:" << endl;
        cin >> _x;
        cout << "������� �������� ���������� y:" << endl;
        cin >> _y;
        cout << "������� �������� ���������� z:" << endl;
        cin >> _z;
        result = result_Poly.calc(_x, _y, _z);
        cout << result << endl;
        break;
    case 'N':
        break;
    default:
        cout << "Error!";
    }
}
#endif