#include <string>
#include "..\include\Polinom.h"

TPolinom::TPolinom(int monoms[][2], int km)
{
    PTMonom elem = new TMonom(0, -1);

    pHead->SetDatValue(elem);

    for (int i = 0; i < km; i++) {
        elem = new TMonom(monoms[i][0], monoms[i][1]);
        InsLast(elem);
    }
}

TPolinom::TPolinom(TPolinom & q)
{
    *this = q;
}

TPolinom TPolinom::operator+(TPolinom & q)
{
    TPolinom result;
    PTMonom resultMonom = nullptr;
    TMonom mon, mon_q;
    const TMonom null(0, -1);

    Reset();
    q.Reset();

    while (!IsListEnded() || !q.IsListEnded()) {
        mon = IsListEnded() ? null : *GetMonom();
        mon_q = q.IsListEnded() ? null : *q.GetMonom();

        if (mon == null) {
            resultMonom = new TMonom(mon_q.GetCoeff(), mon_q.GetIndex());
            q.GoNext();
        }
        else if (mon_q == null) {
            resultMonom = new TMonom(mon.GetCoeff(), mon.GetIndex());
            GoNext();
        }

        else if (mon < mon_q) {
            resultMonom = new TMonom(mon.GetCoeff(), mon.GetIndex());
            GoNext();
        }
        else if (mon_q < mon) {
            resultMonom = new TMonom(mon_q.GetCoeff(), mon_q.GetIndex());
            q.GoNext();
        }

        else if (mon.GetIndex() == mon_q.GetIndex()) {
            resultMonom = new TMonom(mon.GetCoeff() + mon_q.GetCoeff(), mon.GetIndex());
            GoNext();
            q.GoNext();
        }

        else
            break;

        if (resultMonom->GetCoeff())
            result.InsLast(resultMonom);
    }

    return result;
}

TPolinom & TPolinom::operator=(TPolinom & q)
{
    DelList();
 
    if ((&q != nullptr) && (&q != this)) {
        PTMonom Mon = new TMonom(0, -1);
        pHead->SetDatValue(Mon);

        for (q.Reset(); !q.IsListEnded(); q.GoNext()) {
            Mon = q.GetMonom();
            InsLast(Mon->GetCopy());
        }
    }
    
    return *this;
}

bool TPolinom::operator==(TPolinom &q)
{
    bool result = false;

    if (q.GetListLength() == GetListLength()) {
        result = true;
        for (q.Reset(), Reset(); !q.IsListEnded(); q.GoNext(), GoNext())
            if (*(PTMonom)q.GetDatValue() != *(PTMonom)GetDatValue()) {
                result = false;
                break;
            }
    }
    return result;
}

TPolinom TPolinom::Diff(Variables var)
{
    TPolinom result;
    PTMonom resultMonom = nullptr;
    int coeff, index;
    int temp;  // ������� ���������������� ����������

    for (Reset(); !IsListEnded(); GoNext()) {
        coeff = GetMonom()->GetCoeff();
        index = GetMonom()->GetIndex();

        switch (var) {
        case x:
            temp = index / 100;
            index -= 100;
            break;

        case y:
            temp = (index % 100) / 10;
            index -= 10;
            break;

        case z:
            temp = index % 10;
            index -= 1;
            break;

        default:
            break;
        }

        if (temp != 0) {
            coeff *= temp;
            resultMonom = new TMonom(coeff, index);
            result.InsLast(resultMonom);
        }
    }

    return result;
}

TPolinom TPolinom::Integral(Variables var)
{
    TPolinom result;
    PTMonom resultMonom = nullptr;
    int coeff, index;
    int temp;  // ������� ������������� ����������

    for (Reset(); !IsListEnded(); GoNext()) {
        coeff = GetMonom()->GetCoeff();
        index = GetMonom()->GetIndex();

        switch (var) {
        case x:
            temp = index / 100;
            index += 100;
            break;

        case y:
            temp = (index % 100) / 10;
            index += 10;
            break;

        case z:
            temp = index % 10;
            index += 1;
            break;

        default:
            break;
        }

        if (temp > 0 && temp < 9) {
            coeff /= temp + 1;
            resultMonom = new TMonom(coeff, index);
            result.InsLast(resultMonom);
        }
    }

    return result;
}

double TPolinom::Calc(double x, double y, double z)
{
    double result = 0.0;
    int coeff, index;
    int index_x, index_y, index_z;

    for (Reset(); !IsListEnded(); GoNext()) {
        coeff = GetMonom()->GetCoeff();
        index = GetMonom()->GetIndex();

        index_x = index / 100;
        index_y = (index % 100) / 10;
        index_z = index % 10;

        result += coeff * pow(x, index_x) * pow(y, index_y) * pow(z, index_z);
    }

    return result;
}
