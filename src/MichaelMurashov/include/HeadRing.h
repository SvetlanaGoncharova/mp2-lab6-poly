#pragma once

#include "DatList.h"

class THeadRing : public TDatList {
public:
    THeadRing();
    ~THeadRing();
    
    virtual void InsFirst(PTDatValue pVal = nullptr);  // после заголовка                                           
    virtual void DelFirst(void);  // удалить первое звено

protected:
    PTDatLink pHead;  // заголовок, pFirst - звено за pHead
};
