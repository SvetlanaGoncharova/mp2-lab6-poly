#include "gtest/gtest.h"
#include "Polinom.h"
#include "DatList.h"

TEST(TPolinom, Can_Create_Polynom_From_One_Monom)
{
    int mon[][2] = { { 5, 4 } };
    TPolinom Pol(mon, 1);
    TMonom res(5, 4);
    EXPECT_EQ(res, *Pol.GetMonom());
}

TEST(TPolinom, Can_Creation_of_Polynom_of_Four_Monomers)
{
    const int size = 4;
    int mon[][2] = { { 4, 2 }, { 6, 3 }, { 1, 44 }, { 7, 90 }, { 5, 144 } };
    TPolinom Pol(mon, size);
    TMonom monoms[size];
    for (int i = 0; i < size; i++)
        monoms[i] = TMonom(mon[i][0], mon[i][1]);
    for (int i = 0; i < size; i++, Pol.GoNext())
        EXPECT_EQ(monoms[i], *Pol.GetMonom());
}

TEST(TPolinom, �an_�ompare_the_Polynoms)
{
    const int size = 3;
    int mon[][2] = { { 1, 3 },{ 2, 5 },{ 2, 100 } };
    TPolinom Pol1(mon, size);
    TPolinom Pol2(mon, size);
    EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, Can_Assign_Polynoms)
{
    const int size = 2;
    int mon[][2] = { { 5, 8 },{ 2, 4 } };
    TPolinom Pol1(mon, size);
    TPolinom Pol2;
    Pol2 = Pol1;
    EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, Can_Copy_the_Polynoms)
{
    const int size = 2;
    int mon[][2] = { { 5, 8 },{ 2, 4 } };
    TPolinom Pol1(mon, size);
    TPolinom Pol2 = Pol1;
    EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, Can_Add_Up_Linear_Polynoms)
{
    const int size = 1;
    int mon1[][2] = { { 5, 1 } };
    int mon2[][2] = { { 1, 1 } };
    TPolinom Pol1(mon1, size); // 5z
    TPolinom Pol2(mon2, size);// z
    TPolinom Pol = Pol1 + Pol2;
    const int expected_size = 1;
    int expected_mon[][2] = { { 6, 1 } };
    TPolinom expected_Pol(expected_mon, expected_size);
    EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, Can_Add_Up_Polynoms)
{
    const int size1 = 5;
    const int size2 = 4;
    int mon1[][2] = { { 5, 213 },{ 8, 321 },{ 10, 432 },{ -21, 500 },{ 10, 999 } };
    int mon2[][2] = { { 15, 0 },{ -8, 321 },{ 1, 500 },{ 20, 702 } };
    TPolinom Pol1(mon1, size1); // 5x^2yz^3+8x^3y^2z+10x^4y^3z^2-21x^5+10x^9y^9z^9
    TPolinom Pol2(mon2, size2); // 15-8x^3y^2z+x^5+20x^7z^2
    TPolinom Pol = Pol1 + Pol2;
    const int expected_size = 6;
    int expected_mon[][2] = { { 15, 0 },{ 5, 213 },{ 10, 432 },{ -20, 500 },{ 20, 702 },{ 10, 999 } };
    // 15+5x^2yz^3+10x^4y^3z^2-20x^5+20x^7z^2+10x^9y^9z^9
    TPolinom expected_Pol(expected_mon, expected_size);
    EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, Can_Add_Three_Polynoms)
{
    // Arrange
    const int size1 = 2;
    const int size2 = 4;
    const int size3 = 3;
    int mon1[][2] = { { 5, 2 },{ 8, 3 } };
    int mon2[][2] = { { 10, 0 },{ 2, 3 },{ 8, 5 } }; 
    int mon3[][2] = { { 1, 1 },{ -8, 3 },{ 1, 4 },{ 2, 5 } };
    TPolinom Pol1(mon1, size1); // 5z^2+8z^3
    TPolinom Pol2(mon2, size3); // 10+2z^3+8z^5
    TPolinom Pol3(mon3, size2); // z-8z^3+z^4+2z^5
    TPolinom Pol = Pol1 + Pol2 + Pol3;
    const int expected_size = 6;
    int expected_mon[][2] = { { 10, 0 },{ 1, 1 },{ 5, 2 },{ 2, 3 },{ 1, 4 },{ 10, 5 } };
    // z+5z^2+z^4+2z^5
    TPolinom expected_Pol(expected_mon, expected_size);
    EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, Can_Take_the_Derivative_of_Polynom)
{
    const int size = 3;
    int mon[][2] = { { 5, 2 },{ 8, 3 },{ 9, 4 } };
    TPolinom Pol(mon, size); // 5z^2+8z^3+9z^4
    TPolinom diff_Pol = Pol.Diff(z);
    const int expected_size = 3;
    int expected_mon[][2] = { { 10, 1 },{ 24, 2 },{ 36, 3 } };
    // 10z24z^2+36z^3
    TPolinom expected_Pol(expected_mon, expected_size);
    EXPECT_TRUE(diff_Pol == expected_Pol);
}

TEST(TPolinom, Can_Take_the_Derivativ_Polynoms_Over_the_Variable_X)
{
    const int size = 4;
    int mon[][2] = { { 15, 0 },{ -8, 321 },{ 1, 500 },{ 20, 702 } };
    // 15-8x^3y^2z+x^5+20x^7z^2
    TPolinom Pol(mon, size);
    TPolinom diff_Pol = Pol.Diff(x);
    const int expected_size = 3;
    int expected_mon[][2] = { { -24, 221 },{ 5, 400 },{ 140, 602 } };
    // -24x^2y^2z + 5x^4 + 140x^6z^2
    TPolinom expected_Pol(expected_mon, expected_size);
    EXPECT_TRUE(diff_Pol == expected_Pol);
}

TEST(TPolinom, Can_Take_the_Derivativ_Polynoms_Over_the_Variable_Y)
{
    const int size = 4;
    int mon[][2] = { { 15, 0 },{ -8, 321 },{ 1, 500 },{ 20, 702 } };
    TPolinom Pol(mon, size); // 15-8x^3y^2z+x^5+20x^7z^2
    TPolinom diff_Pol = Pol.Diff(y);
    const int expected_size = 1;
    int expected_mon[][2] = { { -16, 311 } };
    // -16x^3yz
    TPolinom expected_Pol(expected_mon, expected_size);
    EXPECT_TRUE(diff_Pol == expected_Pol);
}

TEST(TPolinom, Can_Take_the_Derivativ_Polynoms_Over_the_Variable_Z)
{
    // Arrange
    const int size = 4;
    int mon[][2] = { { 15, 0 },{ -8, 321 },{ 1, 500 },{ 20, 702 } };
    // 15-8x^3y^2z+x^5+20x^7z^2
    TPolinom Pol(mon, size);

    // Act
    TPolinom diff_Pol = Pol.Diff(z);

    // Assert
    const int expected_size = 2;
    int expected_mon[][2] = { { -8, 320 },{ 40, 701 } };
    // -8x^3y^2+7x^3z
    TPolinom expected_Pol(expected_mon, expected_size);
    EXPECT_TRUE(diff_Pol == expected_Pol);
}

TEST(TPolinom, Can_Take_the_Integral_of_Polynom)
{
    int mon[][2] = { { 9, 2 },{ 8, 3 },{ 10, 4 } };
    // 9z^2 + 8z^3 + 10z^4
    TPolinom Pol(mon, 3);
    TPolinom integral_Pol = Pol.Integral(z);
    int expected_mon[][2] = { { 3, 3 },{ 2, 4 },{ 2, 5 } };
    // 3z^3 + 2z^4 + 2z^5
    TPolinom expected_Pol(expected_mon, 3);
    EXPECT_TRUE(integral_Pol == expected_Pol);
}

// Testing type (int)
typedef struct ValueInt :TDatValue
{
    int Value;
    ValueInt() { Value = 0; }
    virtual TDatValue * GetCopy() { TDatValue *temp = new ValueInt; return temp; }
} *PValueInt;

class TestList : public ::testing::Test
{
protected:
    virtual void SetUp()
    {
        // Testing only elem
        Val = new ValueInt();
        Val->Value = 100;

        // Testing ten elems 
        ArrVal = new PValueInt[N];
        for (int i = 0; i < N; i++)
        {
            ArrVal[i] = new ValueInt();
            ArrVal[i]->Value = i;
        }
    }
    virtual void TearDown()
    {
        delete ArrVal;
    }
    // Arrange
    const int N = 10;
    PValueInt Val;
    PValueInt *ArrVal;
    TDatList List;
};

TEST_F(TestList, Can_Add_An_Item_To_the_End)
{
    List.InsLast(Val);
    PValueInt temp = (PValueInt)List.GetDatValue();
    EXPECT_EQ(Val->Value, temp->Value);
}

TEST_F(TestList, Size_of_List)
{
    for (int i = 0; i < N; i++)
        List.InsLast(ArrVal[i]);
    EXPECT_EQ(List.GetListLength(), N);
}

TEST_F(TestList, Can_Add_Ten_Elements_to_End_on_List)
{
    // Arg
    for (int i = 0; i < N; i++)
        List.InsLast(ArrVal[i]);

    // Assert
    for (int i = 0; i < N; i++, List.GoNext())
        EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[i]->Value);
}