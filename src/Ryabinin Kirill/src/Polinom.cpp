#include <string>
#include "Polinom.h"

//����������� � �������� ���������� ������� � ��������
TPolinom::TPolinom(int monoms[][2], int km)
{
    pHead->SetDatValue(new TMonom(0, -1));
    for (int i = 0; i < km; i++)
    {
        InsLast(new TMonom(monoms[i][0], monoms[i][1]));
    }
}

//����������� �����������
TPolinom::TPolinom(TPolinom & q)
{
    pHead->SetDatValue(new TMonom(0, -1));
    for (q.Reset(); !q.IsListEnded(); q.GoNext())
    {
        InsLast(q.GetMonom()->GetCopy());
    }
}

//�������� ���������
TPolinom TPolinom::operator+(TPolinom & q)
{
    TPolinom result;
    PTMonom resultMonom = NULL;
    TMonom mon, mon_q;
    const TMonom null(0, -1);

    Reset();
    q.Reset();

    while (!IsListEnded() || !q.IsListEnded()) {
        mon = IsListEnded() ? null : *GetMonom();
        mon_q = q.IsListEnded() ? null : *q.GetMonom();

        if (mon == null) {
            resultMonom = new TMonom(mon_q.GetCoeff(), mon_q.GetIndex());
            q.GoNext();
        }
        else if (mon_q == null) {
            resultMonom = new TMonom(mon.GetCoeff(), mon.GetIndex());
            GoNext();
        }

        else if (mon < mon_q) {
            resultMonom = new TMonom(mon.GetCoeff(), mon.GetIndex());
            GoNext();
        }
        else if (mon_q < mon) {
            resultMonom = new TMonom(mon_q.GetCoeff(), mon_q.GetIndex());
            q.GoNext();
        }

        else if (mon.GetIndex() == mon_q.GetIndex()) {
            resultMonom = new TMonom(mon.GetCoeff() + mon_q.GetCoeff(), mon.GetIndex());
            GoNext();
            q.GoNext();
        }

        else
            break;

        if (resultMonom->GetCoeff())
            result.InsLast(resultMonom);
    }

    return result;
}

TPolinom & TPolinom::operator=(TPolinom & q)
{
    DelList();

    if ((&q != NULL) && (&q != this)) {
        PTMonom Mon = new TMonom(0, -1);
        pHead->SetDatValue(Mon);

        for (q.Reset(); !q.IsListEnded(); q.GoNext()) {
            Mon = q.GetMonom();
            InsLast(Mon->GetCopy());
        }
    }

    return *this;
}

bool TPolinom::operator==(TPolinom &q)
{
    bool result = false;

    if (q.GetListLength() == GetListLength()) {
        result = true;
        for (q.Reset(), Reset(); !q.IsListEnded(); q.GoNext(), GoNext())
            if (*(PTMonom)q.GetDatValue() != *(PTMonom)GetDatValue()) {
                result = false;
                break;
            }
    }
    return result;
}

TPolinom TPolinom::Diff(Variables xyz)
{
    TPolinom result;
    PTMonom resultMonom = NULL;
    int coeff, index;
    int temp;  

    for (Reset(); !IsListEnded(); GoNext()) {
        coeff = GetMonom()->GetCoeff();
        index = GetMonom()->GetIndex();

        switch (xyz) {
        case x:
            temp = index / 100;
            index -= 100; break;
        case y:
            temp = (index % 100) / 10;
            index -= 10; break;
        case z:
            temp = index % 10;
            index -= 1; break;
        default:
            break;
        }

        if (temp != 0) {
            coeff *= temp;
            resultMonom = new TMonom(coeff, index);
            result.InsLast(resultMonom);
        }
    }

    return result;
}

TPolinom TPolinom::Integral(Variables xyz)
{
    TPolinom result;
    PTMonom resultMonom = NULL;
    int coeff, index;
    int temp;  

    for (Reset(); !IsListEnded(); GoNext()) {
        coeff = GetMonom()->GetCoeff();
        index = GetMonom()->GetIndex();

        switch (xyz) {
        case x:
            temp = index / 100;
            index += 100; break;
        case y:
            temp = (index % 100) / 10;
            index += 10; break;
        case z:
            temp = index % 10;
            index += 1;break;
        default:
            break;
        }

        if (temp > 0 && temp < 9) {
            coeff /= temp + 1;
            resultMonom = new TMonom(coeff, index);
            result.InsLast(resultMonom);
        }
    }
    return result;
}

ostream & operator<<(ostream & os, TPolinom & q)
{
    // ��������� �������, ������� ��������� ������ � ������
    auto CovertIndex = [](int index)
    {
        int tmp;
        string result;
        if (tmp = index / 100)
            result += tmp > 1 ? "x^" + lexical_cast <string> (tmp) : "x";
        if (tmp = index % 100 / 10)
            result += tmp > 1 ? "y^" + lexical_cast <string> (tmp) : "y";
        if (tmp = index % 10)
            result += tmp > 1 ? "z^" + lexical_cast <string> (tmp) : "z";
        return result;
    };
    // ��������� ���� ������. ������� � ����� ���������
    q.Reset();
    os << q.GetMonom()->GetCoeff() << CovertIndex(q.GetMonom()->GetIndex());
    q.GoNext();
    for (; !q.IsListEnded(); q.GoNext())
        os << (q.GetMonom()->GetCoeff() > 0 ? " +" : " ") << q.GetMonom()->GetCoeff() << CovertIndex(q.GetMonom()->GetIndex());
    return os;
}

istream & operator>>(istream & os, TPolinom & q)
{
    // ��������������� ����������
    int i = 0, coeff, index;
    string buf;

    // ���� "��������"
    os >> buf;

    try
    {
        PTMonom mon;
        // ��������� ��������� �� ���������� ������� ������
        regex re_input("(([\\+|-]|^)[1-9](\\d+)?x\\^\\dy\\^\\dz\\^\\d)+");
        // �������� ��� ����������� � ������
        regex re_coeff("^\\d+|(\\+|-)((\\d+))");
        // �������� ��� ������� �� ^
        regex re_index("(\\^\\d)");

        // ���� �� ����������� ������� ����������� ���������, ����������� ����������
        if (!regex_match(buf, re_input)) throw exception("Wrong input format");


        // ��� ���������, �� ������ ��������� ���������� ���������
        inter it_index(buf.begin(), buf.end(), re_index);
        inter it_coeff(buf.begin(), buf.end(), re_coeff);

        // �������� ��� ���������� ��������� � ������ �� ���� ������ �����
        for (; it_coeff != inter(); ++it_coeff, ++i)
        {
            // �������� ������������
            coeff = lexical_cast <int>(it_coeff->str());

            // ������� ����������� ������ ������ x*100+y*10+z*1
            index = lexical_cast <int>(it_index++->str()[1]) * 100 +  //x
                lexical_cast <int>(it_index++->str()[1]) * 10 +       //y
                lexical_cast <int>(it_index++->str()[1]);             //z

                                                                      // ������ ����� � ������� � ����� ������
            mon = new TMonom(coeff, index);
            q.InsLast(mon);
        }
        return os;
    }
    
}